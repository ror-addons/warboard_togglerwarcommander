------------------------------------------------------------------------
r6 | garkin | 2009-08-20 11:48:03 +0000 (Thu, 20 Aug 2009) | 1 line
Changed paths:
   A /tags/0.2 (from /trunk:5)

Tagging as 0.2
------------------------------------------------------------------------
r5 | garkin | 2009-08-20 11:45:04 +0000 (Thu, 20 Aug 2009) | 2 lines
Changed paths:
   M /trunk/WarBoard_TogglerWARCommander.lua

Adjusted tooltip title and window size.

------------------------------------------------------------------------
r4 | garkin | 2009-08-20 08:10:22 +0000 (Thu, 20 Aug 2009) | 2 lines
Changed paths:
   M /trunk/WarBoard_TogglerWARCommander.lua
   M /trunk/WarBoard_TogglerWARCommander.mod

Updated for 1.3.1

------------------------------------------------------------------------
r2 | garkin | 2009-07-25 23:54:45 +0000 (Sat, 25 Jul 2009) | 2 lines
Changed paths:
   A /trunk/.pkgmeta
   A /trunk/LICENSE.txt
   A /trunk/WarBoard_TogglerWARCommander.lua
   A /trunk/WarBoard_TogglerWARCommander.mod

Initial import.

------------------------------------------------------------------------
r1 | root | 2009-07-25 23:45:13 +0000 (Sat, 25 Jul 2009) | 1 line
Changed paths:
   A /branches
   A /tags
   A /trunk

"warboard_togglerwarcommander/mainline: Initial Import"
------------------------------------------------------------------------
